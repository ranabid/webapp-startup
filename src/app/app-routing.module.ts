import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  /* { path: '**', component: PageNotFoundComponent } */
];

@NgModule({
  imports: [
    CommonModule, 
    RouterModule.forRoot(
      routes,
      //{ enableTracing: true }
    )
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
