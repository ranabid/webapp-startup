export class User {
    id: string;
    name: string;
}

export class Video {
    id: number;
    name: string;
}

export class Playlist {
    id: number;
    playlistName: string;
    videos: Video[];
}
export const VIDEOS = [
    {id: 435, name: 'FoxTed Part01'}, 
    {id: 679, name: 'FoxTed Part02'}
  ];
export const PLAYLISTS = [
    { id: 324, playlistName: 'MyList', videos: [{id: 209, name: 'video-209'}, {id: 245, name: 'video-245'}]},
    { id: 452, playlistName: 'MyPlaylist23', videos: [{id: 980, name: 'video-980'}, {id: 401, name: 'video-401'}]}
]
