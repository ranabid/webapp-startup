import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule }        from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DashboardContentComponent } from './dashboard-content/dashboard-content.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { VideoStreamComponent } from './video-stream/video-stream.component';
import { FavoriteComponent } from './favorite.component';
import { UploadedVideosComponent } from './uploaded-videos.component';
import { SubscribedApplicationComponent } from './subscribed-application.component';
import { SubscribedGroupComponent } from './subscribed-group.component';
const routes = [
  { path: 'dashboard', 
    component: DashboardComponent,
    children: [
      {path: '', redirectTo: 'default', pathMatch: 'full'},
      {path: 'default', component: DashboardContentComponent, pathMatch: 'full'},
      { path: 'favorite/videos', component: FavoriteComponent, pathMatch: 'full'},
      { path: 'uploaded/videos', component: UploadedVideosComponent, pathMatch: 'full'},
      { path: 'subscribedapplication/videos', component: SubscribedApplicationComponent, pathMatch: 'full'},
      { path: 'subscribedgroup/videos', component: SubscribedGroupComponent, pathMatch: 'full'}, 
      { path: 'playlist/:id', component: PlaylistComponent, pathMatch: 'full'},
      {path: 'videostream', component: VideoStreamComponent, pathMatch: 'full'}
    ]},
  
];

@NgModule({
  imports: [
    CommonModule, 
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class DashboardRoutingModule { }
