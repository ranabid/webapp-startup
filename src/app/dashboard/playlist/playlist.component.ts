import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { DashboardService } from '../dashboard.service';
import { MessageService } from '../../message/message.service';
import { Playlist, Video } from '../../app.model';

@Component({
  selector: 'app-playlist',
  template: ` <app-dashboard-content [videos]="videos"></app-dashboard-content> `,
  styles: []
})
export class PlaylistComponent implements OnInit {
  videos: Video[];
  playlist: Playlist;
  selectedId: number;
  constructor(private dashboardService: DashboardService, private messageService: MessageService, private route: ActivatedRoute, private router: Router) {
    
   }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.dashboardService.getPlaylist(id).subscribe(data => this.playlist = data);
    this.videos = this.playlist.videos;
    console.log("rana - "+id);
    this.dashboardService.setSection({sectionId: 6,sectionName: "Playlist - "+ this.playlist.playlistName});
    
  }

}
