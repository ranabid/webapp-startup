import { Injectable } from '@angular/core';
import { MessageService } from '../message/message.service';
import { User, Video, Playlist,VIDEOS, PLAYLISTS } from '../app.model';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class DashboardService {
  public videoElement: any;
  videos: Video[];
  private section: any;
  constructor(private messageService: MessageService) { }

  videoControlSetup(videoId: string) {
    this.videoElement = <HTMLVideoElement> document.getElementById(videoId);
    let source = document.createElement('source');
    source.setAttribute('src', 'http://localhost:9998/listen');
    this.videoElement.appendChild(source);
  }

  fetchUploadedVideos(): Observable<Video[]>{
    this.messageService.add("inside dashboard service: fetchUploadedVideos");
    this.videos = VIDEOS;
    return of(this.videos);
  }

  fetchFavoriteVideos(): Observable<Video[]> {
    this.messageService.add("inside dashboard service: fetchFavoriteVideos");
    this.videos = VIDEOS;
    return of(this.videos);
  }

  fetchSubscribedApplicationVideos(): Observable<Video[]> {
    this.messageService.add("inside dashboard service: fetchSubscribedApplication");
    this.videos = VIDEOS;
    return of(this.videos);
  }

  fetchSubscribedGroupsVideos(): Observable<Video[]> {
    this.messageService.add("inside dashboard service: fetchSubscribedGroups");
    this.videos = VIDEOS;
    return of(this.videos);
  }

  fetchDefaultVideos(): Observable<Video[]> {
    this.messageService.add("inside dashboard service: fetchDefaultVideos");  
    this.videos = VIDEOS;
    return of(this.videos);
  }

  getLoginUserDetails(): User {
    this.messageService.add("inside dashboard service: getLoginUserDetails");
    return;
  }

  getPlaylist(id: number | string): Observable<Playlist> { 
    let playlist: Playlist;
    this.messageService.add("playlist id: "+id);
    playlist = PLAYLISTS.find(data => data.id == id)
    return of(playlist);
  } 

  getSection(): any {
    return this.section;
  }
  setSection(section): void {
    this.section = section;
  }

}
