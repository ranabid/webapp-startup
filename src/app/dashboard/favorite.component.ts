import { Component, OnInit } from '@angular/core';
import { Video } from '../app.model';
import { DashboardService } from './dashboard.service';
import { MessageService } from './../message/message.service';
@Component({
  selector: 'app-favorite',
  template: ` <app-dashboard-content [videos]="videos"></app-dashboard-content>`,
  styles: []
})
export class FavoriteComponent implements OnInit {
  videos: Video[];
  constructor(private dashboardService: DashboardService, private messageService: MessageService) { }

  ngOnInit() {
    // fetch favorite videos
    this.dashboardService.fetchFavoriteVideos().subscribe(data => this.videos = data);
    this.dashboardService.setSection({sectionId: 3,sectionName: "My favorites videos"});
  }

}
