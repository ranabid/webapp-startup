import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { MessageService } from './../message/message.service';
import { Video } from '../app.model';

@Component({
  selector: 'app-uploaded-videos',
  template: ` <app-dashboard-content [videos]="videos"></app-dashboard-content> `,
  styles: []
})
export class UploadedVideosComponent implements OnInit {
  videos: Video[];
  constructor(private dashboardService: DashboardService, private messageService: MessageService) { }

  ngOnInit() {
    this.dashboardService.fetchUploadedVideos().subscribe(data => this.videos = data);
    this.dashboardService.setSection({sectionId: 2,sectionName: "My uploaded videos"});
  }

}
