import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardService } from './dashboard.service';
import { DashboardComponent } from './dashboard.component';
import { DashboardSidebarComponent } from './dashboard-sidebar/dashboard-sidebar.component';
import { DashboardContentComponent } from './dashboard-content/dashboard-content.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { VideoStreamComponent } from './video-stream/video-stream.component';
import { SubscribedApplicationComponent } from './subscribed-application.component';
import { SubscribedGroupComponent } from './subscribed-group.component';
import { FavoriteComponent } from './favorite.component';
import { UploadedVideosComponent } from './uploaded-videos.component';
@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
  ],
  declarations: [DashboardComponent, DashboardSidebarComponent, DashboardContentComponent, PlaylistComponent, VideoStreamComponent, SubscribedApplicationComponent, SubscribedGroupComponent, FavoriteComponent, UploadedVideosComponent],
  providers: [DashboardService]
})
export class DashboardModule { }
