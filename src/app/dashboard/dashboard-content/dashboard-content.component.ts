import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from '../../message/message.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { DashboardService } from '../dashboard.service';
import { Video } from '../../app.model';

@Component({
  selector: 'app-dashboard-content',
  templateUrl: './dashboard-content.component.html',
  styleUrls: ['./dashboard-content.component.css']
})
export class DashboardContentComponent implements OnInit {
  @Input()
  videos: Video[];
  section: string;
  constructor(private dashboardService: DashboardService, private messageService: MessageService, private route: ActivatedRoute, private router: Router,) { }
  
  ngOnInit() {
    this.messageService.add("inside dashboard content");
    this.section = this.dashboardService.getSection();
    //this.route.paramMap.switchMap((params: ParamMap) =>this.dashboardService.getVideos(params.get('id')));
    //const id = +this.route.snapshot.paramMap.get('id');
    //this.dashboardService.getVideos(id).subscribe(data => this.videos = data);
  }
}
