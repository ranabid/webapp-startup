import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { MessageService } from './../message/message.service';
import { Video } from '../app.model';

@Component({
  selector: 'app-subscribed-application',
  template: ` <app-dashboard-content [videos]="videos"></app-dashboard-content> `,
  styles: []
})
export class SubscribedApplicationComponent implements OnInit {
  videos: Video[];
  constructor(private dashboardService: DashboardService, private messageService: MessageService) { }

  ngOnInit() {
    this.dashboardService.fetchSubscribedApplicationVideos().subscribe(data => this.videos = data);
    this.dashboardService.setSection({sectionId: 4,sectionName: "My subscribed application"});
  }

}
