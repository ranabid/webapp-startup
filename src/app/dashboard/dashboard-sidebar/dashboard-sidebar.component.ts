import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '../../message/message.service';
import { DashboardService } from '../dashboard.service';
import { User, Playlist, PLAYLISTS } from '../../app.model';
@Component({
  selector: 'app-dashboard-sidebar',
  templateUrl: './dashboard-sidebar.component.html',
  styleUrls: ['./dashboard-sidebar.component.css']
})
export class DashboardSidebarComponent implements OnInit {
  user: User;
  playlists: Playlist[];
  constructor(private router: Router, private messageService: MessageService, private dashboardService: DashboardService) { 
    this.playlists = PLAYLISTS;
  }

  ngOnInit() {
    this.user = this.dashboardService.getLoginUserDetails();
    this.messageService.add("inside dashboard sidebar");
  }

  gotoDefaultView() {
    this.messageService.add("inside dashboard sidebar - gotoDefaultView");
    this.router.navigate(['dashboard/default']);
  }

  gotoMyVideos() {
    this.messageService.add("inside dashboard sidebar - gotoMyVideos");
    this.router.navigate(['dashboard/uploaded/videos/']);
  }

  gotoFavoriteVideos() {
    this.messageService.add("inside dashboard sidebar - gotoFavoriteVideos");
    this.router.navigate(['dashboard/favorite/videos']);
  }

  gotoSubscribedApplications() {
    this.messageService.add("inside dashboard sidebar - gotoSubscribedApplications");
    this.router.navigate(['dashboard/subscribedapplication/videos']);
  }

  gotoSubscribedGroups() {
    this.messageService.add("inside dashboard sidebar - gotoSubscribedGroups");
    this.router.navigate(['dashboard/subscribedgroup/videos']);
  }

  showPlaylist(playlistId: number) {
    this.messageService.add("inside dashboard sidebar - showPlaylist - request for playlist id - "+playlistId);
    this.router.navigate(['dashboard/playlist/'+playlistId]);
  }
}
