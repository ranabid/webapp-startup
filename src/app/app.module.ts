import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { HeaderComponent } from './header/header.component';
import { MessageComponent } from './message/message.component';
import { MessageService } from './message/message.service';
import { AppService } from './app.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MessageComponent,
    PageNotFoundComponent
    ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    DashboardModule,
    AngularFontAwesomeModule
  ],
  providers: [MessageService, AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
