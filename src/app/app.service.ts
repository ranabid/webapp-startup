import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message/message.service';

@Injectable()
export class AppService {

  constructor(private http: HttpClient, private messageService: MessageService) { }
  public log(message: string) {
    this.messageService.add('AppService: ' + message);
  }
}
